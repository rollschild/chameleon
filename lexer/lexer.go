package lexer

import (
	"chameleon-lang/token"
)

type Lexer struct {
	input        string
	position     int
	readPosition int
	char         byte
}

func New(input string) *Lexer {
	stream := &Lexer{input: input}
	stream.readChar()
	return stream
}

// method on type Lexer
func (l *Lexer) readChar() {
	if l.readPosition >= len(l.input) {
		l.char = 0
	} else {
		l.char = l.input[l.readPosition]
	}
	l.position = l.readPosition
	l.readPosition += 1
}

func (l *Lexer) peekChar() byte {
	if l.readPosition >= len(l.input) {
		return 0
	} else {
		return l.input[l.readPosition]
	}
}

func (l *Lexer) readIdentifier() string {
	position := l.position
	for isLetter(l.char) {
		l.readChar()
	}
	// array slice syntax: right bound exclusive
	return l.input[position:l.position]
}

func (l *Lexer) readNumber() string {
	position := l.position
	for isDigit(l.char) {
		l.readChar()
	}
	return l.input[position:l.position]
}

func isDigit(ch byte) bool {
	return '0' <= ch && ch <= '9'
}

// this is the place to decide which character can be part of an identifier
func isLetter(ch byte) bool {
	// numbers can be mixed with letters
	// or can they?
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

func (l *Lexer) skipWhitespace() {
	for l.char == ' ' || l.char == '\t' || l.char == '\n' || l.char == '\r' {
		l.readChar()
	}
}

func (lexer *Lexer) NextToken() token.Token {
	var tok token.Token

	lexer.skipWhitespace()

	switch lexer.char {
	case '=':
		if lexer.peekChar() == '=' {
			ch := lexer.char
			lexer.readChar()
			literal := string(ch) + string(lexer.char)
			tok = token.Token{Type: token.EQ, Literal: literal}
		} else {
			tok = generateToken(token.ASSIGN, lexer.char)
		}
	case '-':
		tok = generateToken(token.MINUS, lexer.char)
	case '+':
		tok = generateToken(token.PLUS, lexer.char)
	case '!':
		if lexer.peekChar() == '=' {
			ch := lexer.char
			lexer.readChar() // now new char in Lexer
			literal := string(ch) + string(lexer.char)
			tok = token.Token{Type: token.NOT_EQ, Literal: literal}
		} else {
			tok = generateToken(token.BANG, lexer.char)
		}
	case '*':
		tok = generateToken(token.STAR, lexer.char)
	case '/':
		tok = generateToken(token.SLASH, lexer.char)
	case '<':
		tok = generateToken(token.LESS, lexer.char)
	case '>':
		tok = generateToken(token.GREATER, lexer.char)
	case ',':
		tok = generateToken(token.COMMA, lexer.char)
	case ';':
		tok = generateToken(token.SEMICOLON, lexer.char)
	case ':':
		tok = generateToken(token.COLON, lexer.char)
	case '(':
		tok = generateToken(token.LPAREN, lexer.char)
	case ')':
		tok = generateToken(token.RPAREN, lexer.char)
	case '{':
		tok = generateToken(token.LBRACE, lexer.char)
	case '}':
		tok = generateToken(token.RBRACE, lexer.char)
	case '[':
		tok = generateToken(token.LBRACKET, lexer.char)
	case ']':
		tok = generateToken(token.RBRACKET, lexer.char)
	case '"':
		tok.Type = token.STRING
		tok.Literal = lexer.readString()
	case 0:
		tok.Literal = ""
		tok.Type = token.EOF
	default:
		if isLetter(lexer.char) {
			tok.Literal = lexer.readIdentifier()
			tok.Type = token.LookupIdent(tok.Literal)
			// early exit is required here
			return tok
		} else if isDigit(lexer.char) {
			// number
			tok.Literal = lexer.readNumber()
			tok.Type = token.INT
			return tok
		} else {
			tok = generateToken(token.ILLEGAL, lexer.char)
		}
	}

	lexer.readChar()
	return tok
}

func generateToken(tokenType token.TokenType, char byte) token.Token {
	return token.Token{Type: tokenType, Literal: string(char)}
}

func (lexer *Lexer) readString() string {
	initialPos := lexer.position + 1

	for {
		lexer.readChar()

		if lexer.char == '"' || lexer.char == 0 {
			break
		}
	}

	return lexer.input[initialPos:lexer.position]
}
