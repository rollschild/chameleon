package parser

import (
	"chameleon-lang/ast"
	"chameleon-lang/lexer"
	"fmt"
	"testing"
)

func TestLetStatements(t *testing.T) {
	/*     input := `
	 *     let arg_one = 2037;
	 *     let arg_two = -6;
	 *     let therdVar = 12;
	 *     `
	 *
	 *     lex := lexer.New(input)
	 *     par := New(lex)
	 *
	 *     program := par.ParseProgram()
	 *     checkParserErrors(t, par)
	 *
	 *     if program == nil {
	 *         t.Fatalf("ParseProgram() returned nil!")
	 *     }
	 *     if len(program.Statements) != 3 {
	 *         t.Fatalf(
	 *             "Invalid statements! Should have three statements but received %d!",
	 *             len(program.Statements))
	 *     } */

	tests := []struct {
		input              string
		expectedIdentifier string
		expectedValue      interface{}
	}{
		{"let x = 6;", "x", 6},
		{"let y = true;", "y", true},
		{"let foo = z;", "foo", "z"},
	}

	for _, item := range tests {
		lex := lexer.New(item.input)
		p := New(lex)
		program := p.ParseProgram()
		checkParserErrors(t, p)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain 1 statement(s)! Received %d instead.", len(program.Statements))
		}

		stmt := program.Statements[0]

		if !testLetStatement(t, stmt, item.expectedIdentifier) {
			return
		}

		val := stmt.(*ast.LetStatement).Value
		if !testLiteralExpression(t, val, item.expectedValue) {
			return
		}
	}
}

func TestReturnStatements(t *testing.T) {
	/*     input := `
	 *     return 6;
	 *     return -9;
	 *     return 2037;
	 *     `
	 *
	 *     lex := lexer.New(input)
	 *     par := New(lex)
	 *
	 *     program := par.ParseProgram()
	 *     checkParserErrors(t, par)
	 *
	 *     if program == nil {
	 *         t.Fatalf("ParseProgram() returned nil!")
	 *     }
	 *
	 *     if len(program.Statements) != 3 {
	 *         t.Fatalf("Invalid statements! Should have three but instead received %d", len(program.Statements))
	 *     } */

	tests := []struct {
		input         string
		expectedValue interface{}
	}{
		{"return 6;", 6},
		{"return foobar;", "foobar"},
		{"return false;", false},
	}

	for _, item := range tests {
		lex := lexer.New(item.input)
		p := New(lex)
		program := p.ParseProgram()
		checkParserErrors(t, p)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain %d statement(s)! Received %d instead.", 1, len(program.Statements))
		}

		stmt := program.Statements[0]
		returnStmt, ok := stmt.(*ast.ReturnStatement)

		if !ok {
			t.Fatalf("returnStmt NOT *ast.ReturnStatement! Received %T instead.", stmt)
		}

		if returnStmt.TokenLiteral() != "return" {
			t.Errorf("TokenLiteral NOT `return`! Instead received %q", returnStmt.TokenLiteral())
		}

		if !testLiteralExpression(t, returnStmt.ReturnValue, item.expectedValue) {
			return
		}
	}

}

func checkParserErrors(t *testing.T, p *Parser) {
	errors := p.Errors()
	if len(errors) == 0 {
		return
	}
	t.Errorf("%d errors occurred:", len(errors))

	for _, msg := range errors {
		t.Errorf("%q", msg) // safely escaped
	}

	t.FailNow()
}

func testLetStatement(t *testing.T, s ast.Statement, name string) bool {
	if s.TokenLiteral() != "let" {
		t.Errorf("TokenLiteral should be 'let' but received %q", s.TokenLiteral())
		return false
	}

	letStmt, ok := s.(*ast.LetStatement)

	if !ok {
		t.Errorf("s is not *ast.LetStatement. Received: %T", s)
		return false
	}

	if letStmt.Name.Value != name {
		t.Errorf("Name is not %s; received: %s", name, letStmt.Name.Value)
		return false
	}

	if letStmt.Name.TokenLiteral() != name {
		t.Errorf("Name Literal is not %s; received: %s", name, letStmt.Name.TokenLiteral())
		return false
	}

	fmt.Println("test succeeded!")
	return true
}

func TestIdentifierExpression(t *testing.T) {
	input := "foobar;"

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("Wrong number of statements! Expected 1; received %d instead.", len(program.Statements))
	}

	// ast.ExpressionStatement is dynamic type???
	// this is a TYPE ASSERTION
	// asser that program.Statements[0] is not nil
	// ...and value store in it is type
	// ...*ast.ExpressionStatement
	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not ast.ExpressionStatement! Received %T instead.", program.Statements[0])
	}
	fmt.Printf("stmt: %T\n", stmt)

	identi, ok := stmt.ExpressionValue.(*ast.Identifier)
	if !ok {
		t.Fatalf("Expression not *ast.Identifier! Received %T instead.", stmt.ExpressionValue)
	}

	if identi.Value != "foobar" {
		t.Fatalf("identi.Value NOT %s! Received %s instead.", "foobar", identi.Value)
	}
	if identi.TokenLiteral() != "foobar" {
		t.Fatalf("identi.TokenLiteral() Not %s! Received %s instead.", "foobar", identi.TokenLiteral())
	}
}

func TestIntegerLiteralExpression(t *testing.T) {
	input := "2037;"

	lex := lexer.New(input)
	p := New(lex)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("Wrong number of statements! Expected 1; received %d instead.", len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Expression not *ast.ExpressionStatement! Received %T instead.", stmt.ExpressionValue)
	}

	literal, ok := stmt.ExpressionValue.(*ast.IntegerLiteral)
	if !ok {
		t.Fatalf("Expression not *ast.IntegerLiteral! Received %T instead.", stmt.ExpressionValue)
	}
	if literal.Value != 2037 {
		t.Fatalf("literal.Value NOT %d! Received %d instead.", 2037, literal.Value)
	}
	if literal.TokenLiteral() != "2037" {
		t.Fatalf("literal.TokenLiteral() NOT %s! Received %s instead.", "2037", literal.TokenLiteral())
	}
}

func TestBooleanExpression(t *testing.T) {
	input := "false;"

	lex := lexer.New(input)
	p := New(lex)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("Wrong number of statements! Expected 1; received %d instead.", len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Expression not *ast.ExpressionStatement! Received %T instead.", stmt.ExpressionValue)
	}

	literal, ok := stmt.ExpressionValue.(*ast.Boolean)
	if !ok {
		t.Fatalf("Expression not *ast.Boolean! Received %T instead.", stmt.ExpressionValue)
	}
	if literal.Value != false {
		t.Fatalf("literal.Value NOT %t! Received %t instead.", false, literal.Value)
	}
	if literal.TokenLiteral() != "false" {
		t.Fatalf("literal.TokenLiteral() NOT %s! Received %s instead.", "false", literal.TokenLiteral())
	}

}

func TestParsingPrefixExpressions(t *testing.T) {
	prefixTests := []struct {
		input    string
		operator string
		value    interface{}
	}{
		{"!69", "!", 69},
		{"-2037;", "-", 2037},
		{"!true;", "!", true},
		{"!false", "!", false},
	}

	for _, item := range prefixTests {
		l := lexer.New(item.input)
		p := New(l)
		program := p.ParseProgram()
		checkParserErrors(t, p)

		if len(program.Statements) != 1 {
			t.Fatalf("Wrong number of statements! Should be %d; received %d instead.", 1, len(program.Statements))
		}

		stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("Not ast.ExpressionStatement! Received %T instead.", program.Statements[0])
		}

		expression, ok := stmt.ExpressionValue.(*ast.PrefixExpression)
		if !ok {
			t.Fatalf("stmt is not ast.PrefixExpression! Received %T instead.", stmt.ExpressionValue)
		}
		if expression.Operator != item.operator {
			t.Fatalf("expression.Operator is not '%s'! Received '%s' instead.", item.operator, expression.Operator)
		}
		if !testLiteralExpression(t, expression.ExprVal, item.value) {
			return
		}
	}
}

func TestParsingInfixExpressions(t *testing.T) {
	infixTests := []struct {
		input      string
		leftValue  interface{}
		operator   string
		rightValue interface{}
	}{
		{"2037 + 2037", 2037, "+", 2037},
		{"2037 - 2037", 2037, "-", 2037},
		{"2037 * 2037", 2037, "*", 2037},
		{"2037 / 2037", 2037, "/", 2037},
		{"2037 > 2037", 2037, ">", 2037},
		{"2037 < 2037", 2037, "<", 2037},
		{"2037 == 2037", 2037, "==", 2037},
		{"2037 != 2037", 2037, "!=", 2037},
		{"true == true;", true, "==", true},
		{"true != false", true, "!=", false},
		{"false == false;", false, "==", false},
	}

	for _, item := range infixTests {
		l := lexer.New(item.input)
		p := New(l)
		program := p.ParseProgram()
		checkParserErrors(t, p)

		if len(program.Statements) != 1 {
			t.Fatalf("Num of statements is NOT 1! Received %d instead.", len(program.Statements))
		}

		stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("program.Statements[0] is NOT *ast.ExpressionStatement! Received %T instead.", program.Statements[0])
		}

		if !testInfixExpression(
			t, stmt.ExpressionValue,
			item.leftValue,
			item.operator,
			item.rightValue,
		) {
			return
		}
		/*         expression, ok := stmt.ExpressionValue.(*ast.InfixExpression)
		 *         if !ok {
		 *             t.Fatalf("expression is NOT *ast.InfixExpression! Received %T instead.", stmt.ExpressionValue)
		 *         }
		 *
		 *         if !testIntegerLiteral(t, expression.LeftValue, item.leftValue) {
		 *             return
		 *         }
		 *
		 *         if expression.Operator != item.operator {
		 *             t.Fatalf("expression.Operator is NOT '%s'! Received '%s'.", item.operator, expression.Operator)
		 *         }
		 *
		 *         if !testIntegerLiteral(t, expression.RightValue, item.rightValue) {
		 *             return
		 *         } */
	}
}

func TestOperatorPrecedenceParsing(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{
			"-a * b",
			"((-a) * b)",
		},
		{
			"!-a",
			"(!(-a))",
		},
		{
			"a + b + c",
			"((a + b) + c)",
		},
		{
			"a + b - c",
			"((a + b) - c)",
		},
		{
			"a * b * c",
			"((a * b) * c)",
		},
		{
			"a * b / c",
			"((a * b) / c)",
		},
		{
			"a + b / c",
			"(a + (b / c))",
		},
		{
			"a + b * c + d / e - f",
			"(((a + (b * c)) + (d / e)) - f)",
		},
		{
			"3 + 4; -5 * 5",
			"(3 + 4)((-5) * 5)",
		},
		{
			"5 > 4 == 3 < 4",
			"((5 > 4) == (3 < 4))",
		},
		{
			"5 < 4 != 3 > 4",
			"((5 < 4) != (3 > 4))",
		},
		{
			"3 + 4 * 5 == 3 * 1 + 4 * 5",
			"((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
		},
		{
			"true",
			"true",
		},
		{
			"false;",
			"false",
		},
		{
			"3 > 5 == false",
			"((3 > 5) == false)",
		},
		{
			"6 < 7 == true;",
			"((6 < 7) == true)",
		},
		{
			"1 + (2 + 3) + 4;",
			"((1 + (2 + 3)) + 4)",
		},
		{
			"(3 + 4) * 9",
			"((3 + 4) * 9)",
		},
		{
			"8 / (2 + 6)",
			"(8 / (2 + 6))",
		},
		{
			"-(1 + 19)",
			"(-(1 + 19))",
		},
		{
			"!(true != false)",
			"(!(true != false))",
		},
		{
			"a + add(b * c) + d",
			"((a + add((b * c))) + d)",
		},
		{
			"add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8))",
			"add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)))",
		},
		{
			"divide(x + y + z * p / q + u)",
			"divide((((x + y) + ((z * p) / q)) + u))",
		},
		{
			"a * [4, 3, 2, 1][b * c] * d",
			"((a * ([4, 3, 2, 1][(b * c)])) * d)",
		},
		{
			"add(a * b[2], b[1], 2 * [3, 4][0]);",
			"add((a * (b[2])), (b[1]), (2 * ([3, 4][0])))",
		},
	}

	for _, item := range tests {
		lex := lexer.New(item.input)
		p := New(lex)
		program := p.ParseProgram()
		checkParserErrors(t, p)

		actual := program.String()
		if actual != item.expected {
			t.Errorf("Expected: %s; Received: %s!", item.expected, actual)
		}
	}
}

func testIntegerLiteral(t *testing.T, il ast.Expression, val int64) bool {
	intVal, ok := il.(*ast.IntegerLiteral)
	if !ok {
		t.Errorf("il is not *ast.IntegerLiteral! Received %T instead.", il)
		return false
	}

	if intVal.Value != val {
		t.Errorf("intVal.Value is not %d! Received %d instead.", val, intVal.Value)
		return false
	}
	if intVal.TokenLiteral() != fmt.Sprintf("%d", val) {
		t.Errorf("intVal.TokenLiteral() is not '%d'! Received '%s' instead.", val, intVal.TokenLiteral())
	}

	return true
}

func testIdentifier(t *testing.T, ident ast.Expression, val string) bool {
	identifier, ok := ident.(*ast.Identifier)
	if !ok {
		t.Errorf("ident is not *ast.Identifier! Received %T instead.", ident)
	}

	if identifier.Value != val {
		t.Errorf("identifier.Value is not %s! Received %s instead.", val, identifier.Value)
	}

	if identifier.TokenLiteral() != val {
		t.Errorf("identifier.TokenLiteral is not %s! Received %s instead.", val, identifier.TokenLiteral())
	}

	return true
}

func testLiteralExpression(
	t *testing.T,
	expr ast.Expression,
	expected interface{},
) bool {
	switch value := expected.(type) {
	case int:
		return testIntegerLiteral(t, expr, int64(value))
	case int64:
		return testIntegerLiteral(t, expr, value)
	case string:
		return testIdentifier(t, expr, value)
	case bool:
		return testBooleanLiteral(t, expr, value)
	}

	t.Errorf("type of expression could not be handled! Received %T.", expr)
	return false
}

func testInfixExpression(
	t *testing.T,
	expr ast.Expression,
	left interface{},
	operator string,
	right interface{},
) bool {
	operExpr, ok := expr.(*ast.InfixExpression)
	if !ok {
		t.Errorf("expr is not ast.InfixExpression! Received %T(%s).", expr, expr)
		return false
	}

	if !testLiteralExpression(t, operExpr.LeftValue, left) {
		return false
	}

	if operExpr.Operator != operator {
		t.Errorf("expr.Operator is not '%s'! Received %q", operator, operExpr.Operator)
		return false
	}

	if !testLiteralExpression(t, operExpr.RightValue, right) {
		return false
	}

	return true
}

func testBooleanLiteral(t *testing.T, expr ast.Expression, val bool) bool {
	bo, ok := expr.(*ast.Boolean)
	if !ok {
		t.Errorf("expr not *ast.Boolean! Received %t instead.", expr)
		return false
	}

	if bo.Value != val {
		t.Errorf("bo.Value not %t! Received %t instead.", val, bo.Value)
		return false
	}

	if bo.TokenLiteral() != fmt.Sprintf("%t", val) {
		t.Errorf("bo.TokenLiteral() not '%t'! Received %s intead.",
			val, bo.TokenLiteral())
		return false
	}

	return true
}

func TestIfExpression(t *testing.T) {
	input := `if (x < y) { x }`

	lex := lexer.New(input)
	p := New(lex)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not have %d statements! Received %d instead.", 1, len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not *ast.ExpressionStatement! Received %T instead.", program.Statements[0])
	}

	expr, ok := stmt.ExpressionValue.(*ast.IfExpression)
	if !ok {
		t.Fatalf("stmt.ExpressionValue is not *ast.IfExpression! Received %T instead.", stmt.ExpressionValue)
	}

	if !testInfixExpression(t, expr.Condition, "x", "<", "y") {
		return
	}

	if len(expr.Consequence.Statements) != 1 {
		t.Errorf("consequence does not contain %d statement(s)! Received %d instead.", 1, len(expr.Consequence.Statements))
	}

	consq, ok := expr.Consequence.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Statements[0] is not *ast.ExpressionStatement! Received %T instead.", expr.Consequence.Statements[0])
	}

	if !testIdentifier(t, consq.ExpressionValue, "x") {
		return
	}

	if expr.Alternative != nil {
		t.Errorf("expr.Alternative.Statements is no nil! Received %+v instead.", expr.Alternative)
	}
}

func TestIfElseExpression(t *testing.T) {
	input := `if (x < y) { x } else {y}`

	lex := lexer.New(input)
	p := New(lex)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not have %d statements! Received %d instead.", 1, len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not *ast.ExpressionStatement! Received %T instead.", program.Statements[0])
	}

	expr, ok := stmt.ExpressionValue.(*ast.IfExpression)
	if !ok {
		t.Fatalf("stmt.ExpressionValue is not *ast.IfExpression! Received %T instead.", stmt.ExpressionValue)
	}

	if !testInfixExpression(t, expr.Condition, "x", "<", "y") {
		return
	}

	if len(expr.Consequence.Statements) != 1 {
		t.Errorf("consequence does not contain %d statement(s)! Received %d instead.", 1, len(expr.Consequence.Statements))
	}

	consq, ok := expr.Consequence.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Statements[0] is not *ast.ExpressionStatement! Received %T instead.", expr.Consequence.Statements[0])
	}

	if !testIdentifier(t, consq.ExpressionValue, "x") {
		return
	}

	if len(expr.Alternative.Statements) != 1 {
		t.Errorf("alternative does not contain %d statement(s)! Received %d instead.", 1, len(expr.Alternative.Statements))
	}

	// test alternative
	altern, ok := expr.Alternative.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Alternative.Statements[0] is not *ast.ExpressionStatement! Received %T instead.", expr.Alternative.Statements[0])
	}

	if !testIdentifier(t, altern.ExpressionValue, "y") {
		return
	}

	/* if expr.Alternative != nil {
	 *     t.Errorf("expr.Alternative.Statements is no nil! Received %+v instead.", expr.Alternative)
	 * } */
}

func TestFunctionLiteralParsing(t *testing.T) {
	input := `fn(x, y) { x + y; }`

	lex := lexer.New(input)
	p := New(lex)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("Num of Statements is not %d! Received %d instead.", 1, len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("stmt is Not *ast.ExpressionStatement! Received %T instead.", program.Statements[0])
	}

	function, ok := stmt.ExpressionValue.(*ast.FunctionLiteral)
	if !ok {
		t.Fatalf("stmt.ExpressionValue is NOT *ast.FunctionLiteral! Received %T instead.", stmt.ExpressionValue)
	}

	if len(function.Parameters) != 2 {
		t.Fatalf("function does not have 2 parameters! Received %d.", len(function.Parameters))
	}

	testLiteralExpression(t, function.Parameters[0], "x")
	testLiteralExpression(t, function.Parameters[1], "y")

	if len(function.Body.Statements) != 1 {
		t.Fatalf("function body does NOT have 1 statement(s)! Received %d.", len(function.Body.Statements))
	}

	bodyStmt, ok := function.Body.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("body statement is NOT *ast.ExpressionStatement! Received %T instead.", function.Body.Statements[0])
	}

	testInfixExpression(t, bodyStmt.ExpressionValue, "x", "+", "y")
}

func TestFunctionParametersParsing(t *testing.T) {
	tests := []struct {
		input          string
		expectedParams []string
	}{
		{input: "fn() {}", expectedParams: []string{}},
		{input: "fn(y) {}", expectedParams: []string{"y"}},
		{input: "fn(a, b, c) {}", expectedParams: []string{"a", "b", "c"}},
	}

	for _, test := range tests {
		lex := lexer.New(test.input)
		p := New(lex)
		program := p.ParseProgram()
		checkParserErrors(t, p)

		stmt := program.Statements[0].(*ast.ExpressionStatement)
		function := stmt.ExpressionValue.(*ast.FunctionLiteral)

		if len(function.Parameters) != len(test.expectedParams) {
			t.Errorf("length of parameters is wrong! Received %d; should be %d.", len(function.Parameters), len(test.expectedParams))
		}

		for idx, ident := range test.expectedParams {
			testLiteralExpression(t, function.Parameters[idx], ident)
		}
	}
}

func TestCallExpressionParsing(t *testing.T) {
	input := `add(1, 2 * 3, 4 + 5);`

	lex := lexer.New(input)
	p := New(lex)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	if len(program.Statements) != 1 {
		t.Fatalf("Num of Statements is not %d! Received %d instead.", 1, len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("stmt is Not *ast.ExpressionStatement! Received %T instead.", program.Statements[0])
	}

	expression, ok := stmt.ExpressionValue.(*ast.CallExpression)
	if !ok {
		t.Fatalf("stmt.ExpressionValue is NOT *ast.CallExpression! Received %T instead.", stmt.ExpressionValue)
	}

	if !testIdentifier(t, expression.Function, "add") {
		return
	}

	if len(expression.Arguments) != 3 {
		t.Fatalf("call expression does not have 3 arguments! Received %d.", len(expression.Arguments))
	}

	testLiteralExpression(t, expression.Arguments[0], 1)
	testInfixExpression(t, expression.Arguments[1], 2, "*", 3)
	testInfixExpression(t, expression.Arguments[2], 4, "+", 5)
}

func TestStringLiteralExpression(t *testing.T) {
	input := `"Hello, world!"`

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	stmt := program.Statements[0].(*ast.ExpressionStatement)
	stringLiteral, ok := stmt.ExpressionValue.(*ast.StringLiteral)
	if !ok {
		t.Fatalf("expression is NOT *ast.StringLiteral: received %T instead!", stmt.ExpressionValue)
	}

	if stringLiteral.Value != "Hello, world!" {
		t.Errorf("string literal is NOT %q: received %q instead!", "Hello, world!", stringLiteral.Value)
	}
}

func TestParsingArrayLiterals(t *testing.T) {
	input := "[12, 2037 * 1, 2037 - 1];"

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	array, ok := stmt.ExpressionValue.(*ast.ArrayLiteral)
	if !ok {
		t.Fatalf("Expression is NOT an ast.ArrayLiteral! Received %T instead!", stmt.ExpressionValue)
	}

	if len(array.Elements) != 3 {
		t.Fatalf("length of array is NOT 3! Received %d elements instead!", len(array.Elements))
	}

	testIntegerLiteral(t, array.Elements[0], 12)
	testInfixExpression(t, array.Elements[1], 2037, "*", 1)
	testInfixExpression(t, array.Elements[2], 2037, "-", 1)
}

func TestParsingIndexExpressions(t *testing.T) {
	input := "sampleArray[2037 - 1];"

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	indexExpression, ok := stmt.ExpressionValue.(*ast.IndexExpression)
	if !ok {
		t.Fatalf("Expression NOT *ast.IndexExpression! Received %T instead!", stmt.ExpressionValue)
	}

	if !testIdentifier(t, indexExpression.Host, "sampleArray") {
		t.Errorf("Host of the index expression is NOT `sampleArray`! Received %q!", indexExpression.Host.String())
	}

	if !testInfixExpression(t, indexExpression.Index, 2037, "-", 1) {
		t.Errorf("the Index Expression inside brackets is NOT `2037 - 1`! Received %q!", indexExpression.Index.String())
	}
}

func TestParsingHashLiteralStringKeys(t *testing.T) {
	input := `{"positive one": 1, "Two": 2, "three": 3}`

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	stmt := program.Statements[0].(*ast.ExpressionStatement)
	hash, ok := stmt.ExpressionValue.(*ast.HashLiteral)
	if !ok {
		t.Fatalf("Expression is NOT a hash literal! Received %T (%+v) instead!", stmt.ExpressionValue, stmt.ExpressionValue)
	}

	if len(hash.Pairs) != 3 {
		t.Errorf("hash.Pairs has wrong number of key-value pairs! Expected 3; received %d!", len(hash.Pairs))
	}

	expectedMap := map[string]int64{
		"positive one": 1,
		"Two":          2,
		"three":        3,
	}

	for key, value := range hash.Pairs {
		// Need to check both key and value!
		keyLiteral, ok := key.(*ast.StringLiteral)
		if !ok {
			t.Errorf("key is not ast.StringLiteral! Received %T instead!", key)
		}

		expectedValue := expectedMap[keyLiteral.String()]
		testIntegerLiteral(t, value, expectedValue)
	}
}

func TestParsingEmptyHashLiteral(t *testing.T) {
	input := "{}"

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	stmt := program.Statements[0].(*ast.ExpressionStatement)
	hash, ok := stmt.ExpressionValue.(*ast.HashLiteral)
	if !ok {
		t.Fatalf("Expression is NOT hash literal! Received %T (%+v) instead!", stmt.ExpressionValue, stmt.ExpressionValue)
	}

	// now hash is HashLiteral
	if len(hash.Pairs) != 0 {
		t.Errorf("hash.Pairs is NOT empty!")
	}
}

func TestParsingHashLiteralsWithExpressoins(t *testing.T) {
	input := `{"positive one": 0 + 1, "Two": 1 * 2, "three": 24 / 8}`

	l := lexer.New(input)
	p := New(l)
	program := p.ParseProgram()
	checkParserErrors(t, p)

	stmt := program.Statements[0].(*ast.ExpressionStatement)
	hash, ok := stmt.ExpressionValue.(*ast.HashLiteral)
	if !ok {
		t.Fatalf("Expression is NOT hash literal! Received %T (%+v) instead!", stmt.ExpressionValue, stmt.ExpressionValue)
	}

	if len(hash.Pairs) != 3 {
		t.Errorf("Wrong number of key-value pairs! Expected 3; received %d", len(hash.Pairs))
	}

	// Now hash is a hash literal
	testFuncMap := map[string]func(ast.Expression){
		"positive one": func(expr ast.Expression) {
			testInfixExpression(t, expr, 0, "+", 1)
		},
		"Two": func(expr ast.Expression) {
			testInfixExpression(t, expr, 1, "*", 2)
		},
		"three": func(expr ast.Expression) {
			testInfixExpression(t, expr, 24, "/", 8)
		},
	}

	for key, value := range hash.Pairs {
		keyLiteral, ok := key.(*ast.StringLiteral)
		if !ok {
			t.Errorf("key is NOT a string literal! Received %T instead!", key)
			// Notice the continue here!
			// there's no need to proceed with the rest of the loop
			continue
		}

		// ready to test
		testFunc, ok := testFuncMap[keyLiteral.String()]
		if !ok {
			t.Errorf("No test function found for key %q!", keyLiteral.String())
			continue
		}

		testFunc(value)
	}
}
