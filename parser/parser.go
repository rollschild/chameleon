package parser

import (
	"chameleon-lang/ast"
	"chameleon-lang/lexer"
	"chameleon-lang/token"
	"fmt"
	"strconv"
	"strings"
)

// Notice the order
// Starting from zero
const (
	_ int = iota
	LOWEST
	EQUALS          // ==
	LESS_OR_GREATER // > or <
	SUM             // +
	PRODUCT         // *
	DIVISION        // /
	PREFIX          // -X or !X
	CALL            // myFunc(X)
	INDEX_OPERATOR  // [
)

var precedences = map[token.TokenType]int{
	token.EQ:       EQUALS,
	token.NOT_EQ:   EQUALS,
	token.LESS:     LESS_OR_GREATER,
	token.GREATER:  LESS_OR_GREATER,
	token.PLUS:     SUM,
	token.MINUS:    SUM,
	token.SLASH:    PRODUCT,
	token.STAR:     PRODUCT,
	token.LPAREN:   CALL,
	token.LBRACKET: INDEX_OPERATOR,
}

var traceLevel int = 0

const traceIdentPlaceholder string = "\t"

func identLevel() string {
	return strings.Repeat(traceIdentPlaceholder, traceLevel-1)
}

func tracePrint(fs string) {
	fmt.Printf("%s%s\n", identLevel(), fs)
}

func incIdent() {
	traceLevel += 1
}
func decIdent() {
	traceLevel -= 1
}

func trace(msg string) string {
	incIdent()
	tracePrint("BEGIN " + msg)
	return msg
}
func untrace(msg string) {
	tracePrint("END " + msg)
	decIdent()
}

func (p *Parser) peekPrecedence() int {
	if prd, ok := precedences[p.peekToken.Type]; ok {
		return prd
	}
	return LOWEST
}
func (p *Parser) currPrecedence() int {
	if prd, ok := precedences[p.currToken.Type]; ok {
		return prd
	}
	return LOWEST
}

type (
	prefixParseFunc func() ast.Expression
	infixParseFunc  func(ast.Expression) ast.Expression
	/* left side of an infix operator^^^ */
)

type Parser struct {
	lex    *lexer.Lexer
	errors []string

	currToken token.Token
	peekToken token.Token

	// for a quick search
	prefixParseFuncMap map[token.TokenType]prefixParseFunc
	infixParseFuncMap  map[token.TokenType]infixParseFunc
}

func (p *Parser) registerPrefix(tokenType token.TokenType, fn prefixParseFunc) {
	p.prefixParseFuncMap[tokenType] = fn
}
func (p *Parser) registerInfix(tokenType token.TokenType, fn infixParseFunc) {
	p.infixParseFuncMap[tokenType] = fn
}

// New
// construct a new Parser
func New(lex *lexer.Lexer) *Parser {
	// return new Parser
	p := &Parser{
		lex:    lex,
		errors: []string{},
	} // notice the address &

	// Register
	// make: to create a slice
	p.prefixParseFuncMap = make(map[token.TokenType]prefixParseFunc)
	p.registerPrefix(token.IDENTIFIER, p.parseIdentifier)
	p.registerPrefix(token.INT, p.parseIntegerLiteral)
	p.registerPrefix(token.BANG, p.parsePrefixExpression)
	p.registerPrefix(token.MINUS, p.parsePrefixExpression)
	p.registerPrefix(token.TRUE, p.parseBoolean)
	p.registerPrefix(token.FALSE, p.parseBoolean)
	p.registerPrefix(token.LPAREN, p.parseGroupedExpression)
	p.registerPrefix(token.IF, p.parseIfExpression)
	p.registerPrefix(token.FUNCTION, p.parseFunctionLiteral)
	p.registerPrefix(token.STRING, p.parseStringLiteral)
	p.registerPrefix(token.LBRACKET, p.parseArrayLiteral)
	p.registerPrefix(token.LBRACE, p.parseHashLiteral)

	p.infixParseFuncMap = make(map[token.TokenType]infixParseFunc)
	p.registerInfix(token.PLUS, p.parseInfixExpression)
	p.registerInfix(token.MINUS, p.parseInfixExpression)
	p.registerInfix(token.STAR, p.parseInfixExpression)
	p.registerInfix(token.SLASH, p.parseInfixExpression)
	p.registerInfix(token.EQ, p.parseInfixExpression)
	p.registerInfix(token.NOT_EQ, p.parseInfixExpression)
	p.registerInfix(token.LESS, p.parseInfixExpression)
	p.registerInfix(token.GREATER, p.parseInfixExpression)
	p.registerInfix(token.LPAREN, p.parseCallExpression)
	p.registerInfix(token.LBRACKET, p.parseIndexExpression)

	// call nextToken() twice to set both
	// ...currToken and peekToken
	p.nextToken()
	p.nextToken()

	return p
}

func (p *Parser) parseHashLiteral() ast.Expression {
	hash := &ast.HashLiteral{Token: p.currToken}
	hash.Pairs = make(map[ast.Expression]ast.Expression) // Notice the *make*

	// currToken is now LBRACE
	for p.peekToken.Type != token.RBRACE {
		p.nextToken()
		// p is now at key position
		key := p.parseExpression(LOWEST)

		if !p.expectPeek(token.COLON) {
			return nil
		}

		// now p is at COLON
		p.nextToken() // now p is at VALUE
		value := p.parseExpression(LOWEST)
		hash.Pairs[key] = value

		if p.peekToken.Type != token.RBRACE && !p.expectPeek(token.COMMA) {
			// now p is at the potential position of a comma
			return nil
		}
	}

	if !p.expectPeek(token.RBRACE) {
		return nil
	}

	return hash
}

func (p *Parser) parseIndexExpression(host ast.Expression) ast.Expression {
	expression := &ast.IndexExpression{Token: p.currToken, Host: host}

	p.nextToken() // now p is at index expression
	expression.Index = p.parseExpression(LOWEST)

	if !p.expectPeek(token.RBRACKET) {
		return nil
		// notice this expectPeek() function already advanced p position!
	}

	return expression
}

func (p *Parser) parseArrayLiteral() ast.Expression {
	array := &ast.ArrayLiteral{Token: p.currToken}
	array.Elements = p.parseExpressionList(token.RBRACKET)
	return array
}

func (p *Parser) parseExpressionList(endToken token.TokenType) []ast.Expression {
	list := []ast.Expression{}

	if p.peekToken.Type == endToken {
		// end of the elements list
		p.nextToken()
		return list
	}

	p.nextToken()
	// now p is at first element

	list = append(list, p.parseExpression(LOWEST))

	for p.peekToken.Type == token.COMMA {
		p.nextToken()
		p.nextToken()
		list = append(list, p.parseExpression(LOWEST))
	}

	if !p.expectPeek(endToken) {
		return nil
	}

	return list
}

func (p *Parser) parseStringLiteral() ast.Expression {
	return &ast.StringLiteral{Token: p.currToken, Value: p.currToken.Literal}
}

func (p *Parser) parseGroupedExpression() ast.Expression {
	// advance to next token to start parsing _real_ stuff
	p.nextToken()

	expr := p.parseExpression(LOWEST)

	if !p.expectPeek(token.RPAREN) {
		return nil

	}

	return expr
}

func (p *Parser) parsePrefixExpression() ast.Expression {
	/* defer untrace(trace("parsePrefixExpression")) */
	// inside this func we call parseExpression() again!!!
	expression := &ast.PrefixExpression{
		Token:    p.currToken,
		Operator: p.currToken.Literal,
	}

	p.nextToken()

	expression.ExprVal = p.parseExpression(PREFIX)

	return expression
}

func (p *Parser) parseInfixExpression(left ast.Expression) ast.Expression {
	/* defer untrace(trace("parseInfixExpression")) */
	expression := &ast.InfixExpression{
		Token:     p.currToken,
		Operator:  p.currToken.Literal,
		LeftValue: left,
	}

	precedence := p.currPrecedence() // of operator
	p.nextToken()                    // right hand side
	expression.RightValue = p.parseExpression(precedence)

	return expression
}

// maybe because Expression is an interface?
func (p *Parser) parseIdentifier() ast.Expression {
	return &ast.Identifier{Token: p.currToken, Value: p.currToken.Literal}
}

func (p *Parser) parseIntegerLiteral() ast.Expression {
	/* defer untrace(trace("parseIntegerLiteral")) */
	lit := &ast.IntegerLiteral{Token: p.currToken}

	value, err := strconv.ParseInt(p.currToken.Literal, 0, 64)
	if err != nil {
		// %q - double quoted string safely escaped
		msg := fmt.Sprintf("Could not parse %q as integer!", p.currToken.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}

	lit.Value = value

	return lit
}

func (p *Parser) parseBoolean() ast.Expression {
	return &ast.Boolean{
		Token: p.currToken,
		Value: p.currToken.Type == token.TRUE,
	}

}

func (p *Parser) Errors() []string {
	return p.errors
}

func (p *Parser) peekError(tok token.TokenType) {
	msg := fmt.Sprintf(
		"Expected next token to be `%s` but received `%s` instead",
		tok, p.peekToken.Type)
	p.errors = append(p.errors, msg)
}

func (p *Parser) nextToken() {
	p.currToken = p.peekToken
	p.peekToken = p.lex.NextToken()
}

func (p *Parser) ParseProgram() *ast.Program {
	// construct root node for AST
	program := &ast.Program{}
	program.Statements = []ast.Statement{}

	for p.currToken.Type != token.EOF {
		stmt := p.parseStatement()
		if stmt != nil {
			program.Statements = append(program.Statements, stmt)
		}
		p.nextToken()
	}
	return program
}

func (p *Parser) parseStatement() ast.Statement {
	switch p.currToken.Type {
	case token.LET:
		return p.parseLetStatement()
	case token.RETURN:
		return p.parseReturnStatement()
	default:
		return p.parseExpressionStatement()
	}
}

func (p *Parser) parseExpressionStatement() *ast.ExpressionStatement {
	/* defer untrace(trace("parseExpressionStatement")) */
	stmt := &ast.ExpressionStatement{Token: p.currToken}
	stmt.ExpressionValue = p.parseExpression(LOWEST)
	/*                            precedence ^^^^^^ */
	if p.peekToken.Type == token.SEMICOLON {
		p.nextToken()
	}
	return stmt
}

func (p *Parser) parseExpression(precedence int) ast.Expression {
	/* defer untrace(trace("parseExpression")) */

	prefix := p.prefixParseFuncMap[p.currToken.Type]
	if prefix == nil {
		p.noPrefixParseFuncError(p.currToken.Type)
		return nil
	}
	leftExpr := prefix() // actual parsing func

	for p.peekToken.Type != token.SEMICOLON && precedence < p.peekPrecedence() {
		infix := p.infixParseFuncMap[p.peekToken.Type]
		if infix == nil {
			return leftExpr
		}

		p.nextToken()
		leftExpr = infix(leftExpr)
	}

	return leftExpr
}

func (p *Parser) parseReturnStatement() *ast.ReturnStatement {
	// currently p is return
	stmt := &ast.ReturnStatement{Token: p.currToken}

	p.nextToken() // now after return

	stmt.ReturnValue = p.parseExpression(LOWEST)

	if p.peekToken.Type == token.SEMICOLON {
		p.nextToken()
	}

	return stmt
}

func (p *Parser) parseLetStatement() *ast.LetStatement {
	stmt := &ast.LetStatement{Token: p.currToken}

	if !p.expectPeek(token.IDENTIFIER) {
		// advanced to next position already
		return nil
	}

	stmt.Name = &ast.Identifier{Token: p.currToken, Value: p.currToken.Literal}

	if !p.expectPeek(token.ASSIGN) {
		return nil
	}

	p.nextToken() // now right hand side of =

	stmt.Value = p.parseExpression(LOWEST)

	if p.peekToken.Type == token.SEMICOLON {
		p.nextToken()
	}

	return stmt
}

func (p *Parser) expectPeek(t token.TokenType) bool {
	if p.peekToken.Type == t {
		p.nextToken()
		return true
	} else {
		p.peekError(t)
		return false
	}
}

func (p *Parser) noPrefixParseFuncError(t token.TokenType) {
	msg := fmt.Sprintf("No prefix parsing function for type %s!", t)
	p.errors = append(p.errors, msg)
}

func (p *Parser) parseIfExpression() ast.Expression {
	expression := &ast.IfExpression{Token: p.currToken}

	// p now is if
	if !p.expectPeek(token.LPAREN) {
		return nil
	}
	// p is now LPAREN

	p.nextToken() // now inside LPAREN
	expression.Condition = p.parseExpression(LOWEST)
	// parseGroupedExpression

	if !p.expectPeek(token.RPAREN) {
		return nil
	} // p now RPAREN

	if !p.expectPeek(token.LBRACE) {
		return nil
	} // p now LBRACE

	expression.Consequence = p.parseBlockStatement()

	if p.peekToken.Type == token.ELSE {
		// there's an else arm
		p.nextToken() // now else

		if !p.expectPeek(token.LBRACE) {
			return nil
		}

		expression.Alternative = p.parseBlockStatement()

	}

	return expression
}

func (p *Parser) parseBlockStatement() *ast.BlockStatement {
	// now p is {
	block := &ast.BlockStatement{Token: p.currToken}
	block.Statements = []ast.Statement{}

	p.nextToken() // now inside block

	for p.currToken.Type != token.RBRACE && p.currToken.Type != token.EOF {
		stmt := p.parseStatement()
		if stmt != nil {
			block.Statements = append(block.Statements, stmt)
		}
		p.nextToken()
	}

	return block
}

func (p *Parser) parseFunctionLiteral() ast.Expression {
	fnLiteral := &ast.FunctionLiteral{Token: p.currToken}

	if !p.expectPeek(token.LPAREN) {
		return nil
	}

	// now p is LPAREN
	fnLiteral.Parameters = p.parseFunctionParameters()

	if !p.expectPeek(token.LBRACE) {
		return nil
	}

	// p is now LBRACE
	fnLiteral.Body = p.parseBlockStatement()

	return fnLiteral
}

func (p *Parser) parseFunctionParameters() []*ast.Identifier {
	parameters := []*ast.Identifier{}

	if p.peekToken.Type == token.RPAREN {
		// empty parameters list
		p.nextToken()
		return parameters
	}

	p.nextToken()
	// now p is the first parameter

	param := &ast.Identifier{Token: p.currToken, Value: p.currToken.Literal}
	parameters = append(parameters, param)

	// now start to parse the rest of the list
	for p.peekToken.Type == token.COMMA {
		// there's another param waiting for us!
		p.nextToken() // comma
		p.nextToken()

		// start processing
		param := &ast.Identifier{Token: p.currToken, Value: p.currToken.Literal}
		parameters = append(parameters, param)
	}

	if !p.expectPeek(token.RPAREN) {
		return nil
	}

	return parameters
}

// the ( in a call expression has the highest precedence
func (p *Parser) parseCallExpression(function ast.Expression) ast.Expression {
	expression := &ast.CallExpression{Token: p.currToken, Function: function}

	/* expression.Arguments = p.parseCallArguments() */
	expression.Arguments = p.parseExpressionList(token.RPAREN)

	return expression
}

/* func (p *Parser) parseCallArguments() []ast.Expression {
 *     args := []ast.Expression{}
 *
 *     if p.peekToken.Type == token.RPAREN {
 *         // no arguments
 *         p.nextToken()
 *         return args
 *     }
 *
 *     p.nextToken()
 *     args = append(args, p.parseExpression(LOWEST))
 *
 *     for p.peekToken.Type == token.COMMA {
 *         p.nextToken()
 *         p.nextToken()
 *         args = append(args, p.parseExpression(LOWEST))
 *     }
 *
 *     if !p.expectPeek(token.RPAREN) {
 *         return nil
 *     }
 *
 *     return args
 * } */
