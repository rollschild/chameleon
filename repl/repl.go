package repl

import (
	"bufio"
	"chameleon-lang/evaluator"
	"chameleon-lang/lexer"
	"chameleon-lang/object"
	"chameleon-lang/parser"
	"fmt"
	"io"
)

const PROMPT = "$=) "

// if first letter of func is upper case, it's exported
// if lower case it's NOT exported
func StartRepl(in io.Reader, out io.Writer) {
	scanner := bufio.NewScanner(in)
	env := object.NewEnvironment()

	for {
		fmt.Printf(PROMPT)
		scanned := scanner.Scan() // it's a boolean
		if !scanned {
			return
			// should we return yet..?
		}
		/* fmt.Printf("%+v\n", scanned) */

		line := scanner.Text()
		lex := lexer.New(line)
		p := parser.New(lex)

		/* for tok := lex.NextToken(); tok.Type != token.EOF; tok = lex.NextToken() {
		 *     fmt.Printf("%+v\n", tok)
		 * } */

		program := p.ParseProgram()
		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			continue
		}

		evaluated := evaluator.Eval(program, env)
		if evaluated != nil {
			io.WriteString(out, evaluated.Inspect())
			io.WriteString(out, "\n")
		}
	}

}

func printParserErrors(out io.Writer, errors []string) {
	io.WriteString(out, "Oops! You did something wrong <3\n")
	io.WriteString(out, "Parser errors:\n")
	for _, msg := range errors {
		io.WriteString(out, "\t"+msg+"\n")
	}
}
