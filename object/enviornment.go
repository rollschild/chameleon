package object

type Environment struct {
	store map[string]Object
	outer *Environment
}

func (env *Environment) Get(name string) (Object, bool) {
	obj, ok := env.store[name]
	// reach to the outer environment
	if !ok && env.outer != nil {
		// notice the syntax
		// NOT :=
		obj, ok = env.outer.Get(name)
	}
	return obj, ok
}

func (env *Environment) Set(name string, val Object) Object {
	env.store[name] = val
	return val
}

func NewEnvironment() *Environment {
	store := make(map[string]Object)
	return &Environment{store: store, outer: nil}
}

func NewEnclosedEnvironment(outerEnv *Environment) *Environment {
	innerEnv := NewEnvironment()
	innerEnv.outer = outerEnv

	return innerEnv
}
