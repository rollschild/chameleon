package object

import "testing"

func TestStringHashKey(t *testing.T) {
	sameStrOne := &String{Value: "Hello, world!"}
	sameStrTwo := &String{Value: "Hello, world!"}
	diffStrOne := &String{Value: "Hello,world!"}
	diffStrTwo := &String{Value: "Hello, World!"}

	if sameStrOne.GenerateHashKey() != sameStrTwo.GenerateHashKey() {
		t.Errorf("strings with same content must have the same key!")
	}

	if sameStrOne.GenerateHashKey() == diffStrOne.GenerateHashKey() {
		t.Errorf("strings with different content should have different hash keys!")
	}

	if sameStrTwo.GenerateHashKey() == diffStrTwo.GenerateHashKey() {
		t.Errorf("strings with different content should have different hash keys!")
	}
}
