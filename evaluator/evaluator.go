package evaluator

import (
	"chameleon-lang/ast"
	"chameleon-lang/object"
	"fmt"
)

var (
	TRUE  = &object.Boolean{Value: true}
	FALSE = &object.Boolean{Value: false}
	NULL  = &object.Null{}
	ZERO  = &object.Integer{Value: 0}
)

func Eval(node ast.Node, env *object.Environment) object.Object {
	// this is a type switch
	switch node := node.(type) {
	case *ast.Program:
		return evalProgram(node, env)
	case *ast.ExpressionStatement:
		return Eval(node.ExpressionValue, env)
	case *ast.IntegerLiteral:
		// println("integer: %T", node.Value)
		return &object.Integer{Value: node.Value}
	case *ast.Boolean:
		// println("boolean: %t", node.Value)
		// return &object.Boolean{Value: node.Value}
		return nativeBool(node.Value)
	case *ast.PrefixExpression:
		operandOnRight := Eval(node.ExprVal, env)
		if isError(operandOnRight) {
			return operandOnRight
		}

		return evalPrefixExpression(node.Operator, operandOnRight)
	case *ast.InfixExpression:
		left := Eval(node.LeftValue, env)
		if isError(left) {
			return left
		}

		right := Eval(node.RightValue, env)
		if isError(right) {
			return right
		}

		return evalInfixExpression(node.Operator, left, right)
	case *ast.BlockStatement:
		return evalBlockStatement(node, env)
	case *ast.IfExpression:
		return evalIfExpresstion(node, env)
	case *ast.ReturnStatement:
		returnValue := Eval(node.ReturnValue, env)
		if isError(returnValue) {
			return returnValue
		}
		return &object.ReturnValue{Value: returnValue}
	case *ast.LetStatement:
		val := Eval(node.Value, env)
		if isError(val) {
			return val
		}
		env.Set(node.Name.Value, val)
		// don't we return something here?
	case *ast.Identifier:
		return evalIdentifier(node, env)
	case *ast.FunctionLiteral:
		params := node.Parameters
		body := node.Body

		return &object.Function{
			Parameters: params,
			Body:       body,
			Env:        env,
		}
	case *ast.CallExpression:
		// node.Function can be FunctionLiteral or Identifier
		function := Eval(node.Function, env)
		if isError(function) {
			return function
		}

		args := evalExpressions(node.Arguments, env)
		if len(args) == 1 && isError(args[0]) {
			// Error occurs!
			return args[0]
		}

		return applyFuncion(function, args)
	case *ast.StringLiteral:
		return &object.String{Value: node.Value}
	case *ast.ArrayLiteral:
		elements := evalExpressions(node.Elements, env)
		if len(elements) == 1 && isError(elements[0]) {
			return elements[0]
		}

		return &object.Array{Elements: elements}
	case *ast.IndexExpression:
		host := Eval(node.Host, env)
		if isError(host) {
			return host
		}

		index := Eval(node.Index, env)
		if isError(index) {
			return index
		}

		return evalIndexExpression(host, index)
	case *ast.HashLiteral:
		return evalHashLiteral(node, env)
	}

	return nil
}

func evalHashLiteral(node *ast.HashLiteral, env *object.Environment) object.Object {
	pairs := make(map[object.HashKey]object.HashPair)

	for keyNode, valueNode := range node.Pairs {
		key := Eval(keyNode, env)
		if isError(key) {
			return key
		}

		// GenerateHashKey()
		hashKey, ok := key.(object.Hashable)
		if !ok {
			return newError("Key %s cannot be used as hash key!", key.Type())
		}

		value := Eval(valueNode, env)
		if isError(value) {
			return value
		}

		hashedKey := hashKey.GenerateHashKey()
		pairs[hashedKey] = object.HashPair{Key: key, Value: value}
	}

	return &object.Hash{Pairs: pairs}
}

func evalIndexExpression(host, index object.Object) object.Object {
	switch {
	case host.Type() == object.ARRAY_OBJ && index.Type() == object.INTEGER_OBJ:
		return evalArrayIndexExpression(host, index)
	case host.Type() == object.HASH_OBJ:
		return evalHashIndexExpression(host, index)
	default:
		return newError("Index operator NOT supported: received %s instead!", index.Type())

	}
}

func evalHashIndexExpression(host, index object.Object) object.Object {
	hashObject := host.(*object.Hash)

	key, ok := index.(object.Hashable)
	if !ok {
		return newError("Key %s cannot be used as hash key!", index.Type())
	}

	pair, ok := hashObject.Pairs[key.GenerateHashKey()]
	if !ok {
		// No such pair
		// but no necessarily an error
		return NULL
	}

	return pair.Value
}

func evalArrayIndexExpression(host, index object.Object) object.Object {
	arrayObj := host.(*object.Array)
	integerObj := index.(*object.Integer)
	idx := integerObj.Value

	// Notice the type cast
	maxIdx := int64(len(arrayObj.Elements) - 1)

	if idx < 0 || idx > maxIdx {
		return NULL
	}

	return arrayObj.Elements[idx]
}

func applyFuncion(fn object.Object, args []object.Object) object.Object {
	switch fn := fn.(type) {
	case *object.Function:
		extendedEnv := extendFunctionEnv(fn, args)
		evaluated := Eval(fn.Body, extendedEnv)
		return unwrapReturnValue(evaluated)
	case *object.Builtin:
		return fn.Fn(args...)
	default:
		return newError("Not a Function: received %s instead", fn.Type())
	}
}

func extendFunctionEnv(fn *object.Function, args []object.Object) *object.Environment {
	env := object.NewEnclosedEnvironment(fn.Env)

	for paramIndex, param := range fn.Parameters {
		// set bindings
		env.Set(param.Value, args[paramIndex])
	}

	return env
}

func unwrapReturnValue(obj object.Object) object.Object {
	if returnedValue, ok := obj.(*object.ReturnValue); ok {
		return returnedValue.Value
	}
	return obj
}

func evalExpressions(exprs []ast.Expression, env *object.Environment) []object.Object {
	var evaluatedExprs []object.Object

	for _, expr := range exprs {
		// every expression
		evaluated := Eval(expr, env)
		if isError(evaluated) {
			// how to determine if an error occurred during the process
			// ...of evaluating one of the expressions?
			// return right away
			return []object.Object{evaluated}
		}
		evaluatedExprs = append(evaluatedExprs, evaluated)
	}

	return evaluatedExprs
}

func evalIdentifier(node *ast.Identifier, env *object.Environment) object.Object {

	if val, ok := env.Get(node.Value); ok {
		return val
	}

	if builtin, ok := builtins[node.Value]; ok {
		return builtin
	}

	return newError("Identifier NOT found: `%s`!", node.Value)
}

func isError(obj object.Object) bool {
	if obj != nil {
		return obj.Type() == object.ERROR_OBJ
	}
	return false
}

func newError(format string, messages ...interface{}) *object.Error {
	return &object.Error{ErrorMessage: fmt.Sprintf(format, messages...)}
}

func evalProgram(node *ast.Program, env *object.Environment) object.Object {
	var result object.Object // here we expect a type

	for _, stmt := range node.Statements {
		result = Eval(stmt, env)

		/* if returnValue, ok := result.(*object.ReturnValue); ok {
		 *     return returnValue.Value
		 * } */

		switch result := result.(type) {
		case *object.ReturnValue:
			return result.Value
		case *object.Error:
			return result
		}
	}

	return result
}

func evalIfExpresstion(node *ast.IfExpression, env *object.Environment) object.Object {
	condition := Eval(node.Condition, env)
	if isError(condition) {
		return condition
	}

	if isTruthy(condition) {
		return Eval(node.Consequence, env)
	} else if node.Alternative != nil {
		return Eval(node.Alternative, env)
	} else {
		return NULL
	}
}

func isTruthy(obj object.Object) bool {
	switch obj {
	case TRUE:
		return true
	case FALSE:
		return false
	case NULL:
		return false
	default:
		return true
	}
}

func evalInfixExpression(operator string, left, right object.Object) object.Object {
	switch {
	case left.Type() == object.INTEGER_OBJ && right.Type() == object.INTEGER_OBJ:
		return evalIntegerInfixExpression(operator, left.(*object.Integer), right.(*object.Integer))
	case left.Type() == object.STRING_OBJ && right.Type() == object.STRING_OBJ:
		return evalStringInfixExpression(operator, left.(*object.String), right.(*object.String))
	case left.Type() == object.BOOLEAN_OBJ && right.Type() == object.BOOLEAN_OBJ:
		// we are using pointer comparison
		// it will work because we always use pointers to objects
		if operator == "==" {
			return nativeBool(left == right)
		} else if operator == "!=" {
			return nativeBool(left != right)
		} else {
			// I added this line...
			return newError("Unknown operator: `%s %s %s`!", left.Type(), operator, right.Type())
		}
	case left.Type() != right.Type():
		return newError("Type mismatch: `%s %s %s`!", left.Type(), operator, right.Type())
	default:
		return NULL
	}
}

func evalStringInfixExpression(operator string, left, right *object.String) object.Object {
	// check if it's any operation other than concatenation
	if operator != "+" {
		return newError("Unknown operator: `%s %s %s`!", left.Type(), operator, right.Type())
	}

	leftStringVal := left.Value
	rightStringVal := right.Value

	return &object.String{Value: leftStringVal + rightStringVal}
}

func evalIntegerInfixExpression(operator string, left, right *object.Integer) object.Object {
	leftVal := left.Value
	rightVal := right.Value

	switch operator {
	case "+":
		return &object.Integer{Value: leftVal + rightVal}
	case "-":
		return &object.Integer{Value: leftVal - rightVal}
	case "*":
		return &object.Integer{Value: leftVal * rightVal}
	case "/":
		return &object.Integer{Value: leftVal / rightVal}
	case "<":
		return nativeBool(leftVal < rightVal)
	case ">":
		return nativeBool(leftVal > rightVal)
	case "==":
		return nativeBool(leftVal == rightVal)
	case "!=":
		return nativeBool(leftVal != rightVal)
	/* case "<=":
	 *     return nativeBool(leftVal <= rightVal)
	 * case ">=":
	 *     return nativeBool(leftVal >= rightVal) */
	default:
		return newError("Unknown operator: `%s %s %s`!", left.Type(), operator, right.Type())
	}
}

func evalPrefixExpression(operator string, operand object.Object) object.Object {
	switch operator {
	case "!":
		return evalBangOperatorExpression(operand)
	case "-":
		return evalMinusPrefixOperatorExpression(operand)
	default:
		return newError("Unknown operator: `%s%s`!", operator, operand.Type())
	}
}

func evalMinusPrefixOperatorExpression(operand object.Object) object.Object {
	if operand.Type() != object.INTEGER_OBJ {
		return newError("Unknown operand: `-%s`!", operand.Type())
	}

	intObj, ok := operand.(*object.Integer)
	if !ok {
		return newError("Unknown operand: `-%s`!", operand)
	}

	value := intObj.Value

	return &object.Integer{Value: value * (-1)}
}

func evalBangOperatorExpression(operand object.Object) object.Object {
	switch operand {
	case TRUE:
		return FALSE
	case FALSE:
		return TRUE
	case ZERO:
		return TRUE
	case NULL:
		return TRUE
	default:
		/* if operand.Inspect() == "0" {
		 *     return TRUE
		 * } */
		return FALSE
	}
}

func nativeBool(input bool) *object.Boolean {
	if input {
		return TRUE
	}
	return FALSE
}

/* func evalStatements(stmts []ast.Statement) object.Object {
 *     var result object.Object
 *
 *     for _, stmt := range stmts {
 *         result = Eval(stmt)
 *
 *         // Break out of the loop immediately if it's a return
 *         if returnValue, ok := result.(*object.ReturnValue); ok {
 *             return returnValue.Value
 *         }
 *     }
 *
 *     return result
 * } */

func evalBlockStatement(node *ast.BlockStatement, env *object.Environment) object.Object {
	var result object.Object

	for _, stmt := range node.Statements {
		result = Eval(stmt, env)

		if result != nil {
			resultType := result.Type()

			if resultType == object.RETURN_VALUE_OBJ ||
				resultType == object.ERROR_OBJ {
				// we do NOT unwrap the return value here
				// ...instead we let it bubble up
				return result
			}
		}
	}

	return result
}
