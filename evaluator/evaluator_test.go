package evaluator

import (
	"chameleon-lang/lexer"
	"chameleon-lang/object"
	"chameleon-lang/parser"
	"testing"
)

func TestEvalHashInndexExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{
			`{"foo": 2037}["foo"]`,
			2037,
		},
		{
			`{"foo": 2037}["bar"]`,
			nil,
		},
		{
			`let key = "foo"; {"foo": -2037}[key]`,
			-2037,
		},
		{
			`{}["foo"]`,
			nil,
		},
		{
			`{-1: -1}[-1]`,
			-1,
		},
		{
			`{true: 2037}[true]`,
			2037,
		},
		{
			`{false: -2037}[false]`,
			-2037,
		},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		expectedVal, ok := test.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(expectedVal))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func TestEvalHashLiterals(t *testing.T) {
	input := `
    let threeStr = "three";
	{
		"one": 100 - 99,
		2: 2,
		threeStr: 9 / 3,
		true: 4,
		false: 5,
	}
	`

	evaluated := testEval(input)
	result, ok := evaluated.(*object.Hash)
	if !ok {
		t.Fatalf("Evaluation did NOT return a Hash! Recevied %T (%+v) instead!", evaluated, evaluated)
	}

	expectedMap := map[object.HashKey]int64{
		(&object.String{Value: "one"}).GenerateHashKey():   1,
		(&object.Integer{Value: 2}).GenerateHashKey():      2,
		(&object.String{Value: "three"}).GenerateHashKey(): 3,
		TRUE.GenerateHashKey():                             4,
		FALSE.GenerateHashKey():                            5,
	}

	if len(result.Pairs) != len(expectedMap) {
		t.Errorf("Hash has wrong number of key-value pairs! Expected: %d; received %d!", len(expectedMap), len(result.Pairs))
	}

	for expectedKey, expectedValue := range expectedMap {
		pair, ok := result.Pairs[expectedKey]
		if !ok {
			t.Errorf("No such pair for given key!")
		}

		testIntegerObject(t, pair.Value, expectedValue)
	}
}

func TestEvalArrayIndexExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{
			"[4, 3, 2, 1][0]",
			4,
		},
		{
			"[4, 3, 2, 1][3]",
			1,
		},
		{
			"let idx = 0; [-1, 0][idx]",
			-1,
		},
		{
			"[4, 3, 2, 1][2 - 1]",
			3,
		},
		{
			"let sampleArray = [4, 3, 1, 2]; sampleArray[2];",
			1,
		},
		{
			"let sampleArray = [4, 3, 2, 1]; sampleArray[0] - sampleArray[1] + sampleArray[3];",
			2,
		},
		{
			"let myArray = [4, 3, 2, 1]; let idx = myArray[3]; myArray[idx];",
			3,
		},
		{
			"[4, 3, 2, 1][4]",
			nil,
		},
		{
			"[4, 3, 2, 1][-1]",
			nil,
		},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		integer, ok := test.expected.(int)

		if ok {
			// expected int
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func TestEvalArrayLiterals(t *testing.T) {
	input := "[12, -2037 * -1, 2037 - 1]"

	evaluated := testEval(input)
	array, ok := evaluated.(*object.Array)
	if !ok {
		t.Fatalf("Object is NOT an Array! Recevied %T (%+v) instead!", evaluated, evaluated)
	}

	if len(array.Elements) != 3 {
		t.Fatalf("Array has wrong number of elements! Expected %d; received %d", 3, len(array.Elements))
	}

	testIntegerObject(t, array.Elements[0], 12)
	testIntegerObject(t, array.Elements[1], 2037)
	testIntegerObject(t, array.Elements[2], 2036)
}

func TestEvalBuiltinFunctions(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{`strlen("")`, 0},
		{`strlen("five")`, 4},
		{`strlen("Hello, world!")`, 13},
		{`strlen("Hello, " + "world!")`, 13},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		if evaluated.Type() != object.INTEGER_OBJ {
			t.Fatalf("Something went wrong... Expected INTEGER but got %T (%+v) instead!", evaluated, evaluated)
		}
		testIntegerObject(t, evaluated, test.expected)
	}
}

func TestEvalStringConcatenation(t *testing.T) {
	input := `"Hello" + ", world" + "!"`

	evaluated := testEval(input)
	str, ok := evaluated.(*object.String)
	if !ok {
		t.Fatalf("object is NOT a String! received %T (%+v) intead.", evaluated, evaluated)
	}

	if str.Value != "Hello, world!" {
		t.Errorf("String has wrong value! received %q instead.", str.Value)
	}
}

func TestEvalStringLiteral(t *testing.T) {
	input := `"Hello, world!"` // this is a string also

	evaluated := testEval(input)
	str, ok := evaluated.(*object.String)
	if !ok {
		t.Fatalf("object is NOT a string: received %T (%+v) instead!", evaluated, evaluated)
	}

	if str.Value != "Hello, world!" {
		t.Errorf("String has wrong value! received %q instead.", str.Value)
	}
}

func TestClosures(t *testing.T) {
	input := `
let newAdder = fn(x) {
	fn(y) {
		x + y;
	};
};
let addThree = newAdder(3);
addThree(-9);
	`
	// here, newAdder is a higher order function
	// they either return other functions or
	// ...receive functions as arguments

	testIntegerObject(t, testEval(input), -6)
}

func TestEvalFunctionObject(t *testing.T) {
	input := "fn(x) { x - 3; };"
	// The () come from *ast.InfixExpression.String()
	expectedFuncBody := "(x - 3)"

	evaluated := testEval(input)

	fnObj, ok := evaluated.(*object.Function)

	if !ok {
		t.Fatalf("object is NOT a Function: received %T (%+v) instead!", evaluated, evaluated)
	}

	if len(fnObj.Parameters) != 1 {
		t.Fatalf("function has wrong parameters: received %+v instead!", fnObj.Parameters)
	}

	if fnObj.Parameters[0].String() != "x" {
		t.Fatalf("parameter is NOT 'x': received %q instead!", fnObj.Parameters[0].String())
	}

	if fnObj.Body.String() != expectedFuncBody {
		t.Fatalf("function body is NOT %q: received %q instead!", expectedFuncBody, fnObj.Body.String())
	}
}

func TestFunctionApplication(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"let id = fn(x) { x; }; id(2037);", 2037},
		{"let id = fn(y) { return y;}; id(-2037);", -2037},
		{"let double = fn(a) { 2 * a; }; double(9);", 18},
		{"let minus = fn(m, n) { m - n; }; minus(12, 7);", 5},
		{"let minus = fn(p, q) { p - q; }; minus(3 + 2, minus(12, 7));", 0},
		{"fn(i) { i; }(6)", 6},
	}

	for _, test := range tests {
		testIntegerObject(t, testEval(test.input), test.expected)
	}
}

func TestEvalLetStatements(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"let a = 6; a;", 6},
		{"let a = 4 * 9; a;", 36},
		{"let a = -2; let b = a; b;", -2},
		{"let a = 2037; let b = a - 1; let c = b + a - 1000; c;", 3073},
	}

	for _, test := range tests {
		testIntegerObject(t, testEval(test.input), test.expected)
	}
}

func TestErrorHandling(t *testing.T) {
	tests := []struct {
		input                string
		expectedErrorMessage string
	}{
		{"5 + true;", "Type mismatch: `INTEGER + BOOLEAN`!"},
		{"5 + false; 5;", "Type mismatch: `INTEGER + BOOLEAN`!"},
		{"-true;", "Unknown operand: `-BOOLEAN`!"},
		{"false - true;", "Unknown operator: `BOOLEAN - BOOLEAN`!"},
		{"13; true + false; 4;", "Unknown operator: `BOOLEAN + BOOLEAN`!"},
		{"if (2037 > -2037) { true + false; }",
			"Unknown operator: `BOOLEAN + BOOLEAN`!"},
		{
			`
if (2037 > -2037) {
	if (-2037 < 2037) {
		return true - false;
	}

	return -1;
}
			`,
			"Unknown operator: `BOOLEAN - BOOLEAN`!",
		},
		{"foobar;", "Identifier NOT found: `foobar`!"},
		{
			`"Hello, " - "world!"`,
			"Unknown operator: `STRING - STRING`!",
		},
		{
			`strlen(2037)`,
			"Argument to `strlen` NOT supported! Expected STRING; received INTEGER instead!",
		},
		{
			`strlen("Hello, ", "world!")`,
			"Wrong number of arguments! Expected: 1; received: 2!",
		},
		{
			`{"keyOne": "chameleon"}[fn(x) { x }]`,
			"Key FUNCTION cannot be used as hash key!",
		},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)

		errObj, ok := evaluated.(*object.Error)
		if !ok {
			t.Errorf("No Error object returned: received %T (%+v) instead!",
				evaluated, evaluated)
			continue
		}

		if errObj.ErrorMessage != test.expectedErrorMessage {
			t.Errorf(
				"Wrong error message: expected %q, but received %q instead!",
				test.expectedErrorMessage, errObj.ErrorMessage)
		}
	}
}

func TestEvalReturnStatements(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"return 2037;", 2037},
		{"return 2037; let x = 9;", 2037},
		{"return 3 + 2 * 5; 2037", 13},
		{"2037; return -2037; 2036", -2037},
		{
			`
			if (2037 > -2037) {
				if (-2037 < 2037) {
					return -2037;
				}

				return 2037;
			}
			`,
			-2037,
		},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		testIntegerObject(t, evaluated, test.expected)
	}
}

func TestEvalIntegerExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"2037; 18", 18},
		{"0", 0},
		{"-6", -6},
		{"-1992", -1992},
		{"5 + 6 + 7 - 10", 8},
		{"2 * 3 * 4", 24},
		{"-6 + 9 - 12", -9},
		{"3 * 4 + 5", 17},
		{"3 + 4 * 5", 23},
		{"20 + 2 * -10", 0},
		{"72 / 3 * 4 + 4", 100},
		{"2 * (6 + 7)", 26},
		{"2 * 4 * 8 + -13", 51},
		{"2 * (4 * 8) - 13", 51},
		{"(3 * (7 - 5) / 2 - 9) * -1", 6},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		testIntegerObject(t, evaluated, test.expected)
	}
}

func TestEvalBooleanExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"true; false", false},
		{"false", false},
		{"2 < 3", true},
		{"2 < -2", false},
		{"4 > -4", true},
		{"4 > 4", false},
		{"2037 == 2037", true},
		{"2037 != 2037", false},
		{"-1 == 1", false},
		{"-1 != -1", false},
		{"true == true", true},
		{"true == false", false},
		{"false != true", true},
		{"true != false", true},
		{"false == false", true},
		/* {"2 >= 2", true},
		 * {"3 <= -3", false}, */
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		testBooleanObject(t, evaluated, test.expected)
	}
}

func TestEvalBangOperator(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"!true", false},
		{"!false", true},
		{"!5", false}, // 5 is truthy
		{"!!true", true},
		{"!!false", false},
		{"!!89", true},
		{"!0", false}, // 0 is NOT falsy
		{"!!0", true}, // 0 is NOT falsy
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		testBooleanObject(t, evaluated, test.expected)
	}
}

func TestIfElseExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{"if (true) { 10 }", 10},
		{"if (false) { 10 }", nil},
		{"if (1) { 10 }", 10},
		{"if (-3 < 3) { 10 }", 10},
		{"if (-3 > 3) { 10 }", nil},
		{"if (6 == 6) { 10 } else { -2 }", 10},
		{"if (6 != 6) { 10 } else {-2}", -2},
	}

	for _, test := range tests {
		evaluated := testEval(test.input)
		expectedIsInt, ok := test.expected.(int)

		if ok {
			testIntegerObject(t, evaluated, int64(expectedIsInt))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func testNullObject(t *testing.T, obj object.Object) bool {
	// NULL is in the same package, evaluator
	// again, compare pointers
	if obj != NULL {
		t.Errorf("object is not NULL! received %T (%+v) instead!", obj, obj)
		return false
	}
	return true
}

func testEval(input string) object.Object {
	lex := lexer.New(input)
	p := parser.New(lex)
	program := p.ParseProgram()
	// a fresh Environment every time testEval is called
	env := object.NewEnvironment()

	return Eval(program, env)
}

func testBooleanObject(t *testing.T, obj object.Object, expected bool) bool {
	result, ok := obj.(*object.Boolean)
	if !ok {
		t.Errorf("object is not Boolean! received %T (%+v) instead", obj, obj)
		return false
	}
	if result.Value != expected {
		t.Errorf("object has wrong value! received %t but expected %t", result.Value, expected)
		return false
	}
	return true
}

func testIntegerObject(t *testing.T, obj object.Object, expected int64) bool {
	// make assertions
	result, ok := obj.(*object.Integer)
	if !ok {
		t.Errorf("the object is not object.Object! received %T (%+v) instead", obj, obj)
		return false
	}
	if result.Value != expected {
		t.Errorf("the object has wrong value! received %d but expected %d", result.Value, expected)
		return false
	}
	return true
}
