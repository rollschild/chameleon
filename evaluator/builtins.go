package evaluator

import (
	"chameleon-lang/object"
	"fmt"
)

var builtins = map[string]*object.Builtin{
	"strlen": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			if len(args) != 1 {
				return newError("Wrong number of arguments! Expected: 1; received: %d!", len(args))
			}

			switch arg := args[0].(type) {
			case *object.String:
				return &object.Integer{Value: int64(len(arg.Value))}
				// do a cast here, remember!
			default:
				return newError("Argument to `strlen` NOT supported! Expected STRING; received %s instead!", arg.Type())
			}
		},
	},
	"arrlen": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			if len(args) != 1 {
				return newError("Wrong number of arguments! Expected: 1; received: %d!", len(args))
			}

			switch arg := args[0].(type) {
			case *object.Array:
				return &object.Integer{Value: int64(len(arg.Elements))}
			default:
				return newError("Argument to `arrlen` NOT supported! Expected ARRAY; received %s instead!", arg.Type())

			}
		},
	},
	"first": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			if len(args) != 1 {
				return newError("Wrong number of arguments! Expected: 1; received: %d!", len(args))
			}

			if args[0].Type() != object.ARRAY_OBJ {
				return newError("Argument to `first()` must be ARRAY! Received %s instead!", args[0].Type())
			}

			arrayObj := args[0].(*object.Array)
			if len(arrayObj.Elements) > 0 {
				return arrayObj.Elements[0]
			}

			return NULL
		},
	},
	"last": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			if len(args) != 1 {
				return newError("Wrong number of arguments! Expected: 1; received: %d!", len(args))
			}

			if args[0].Type() != object.ARRAY_OBJ {
				return newError("Argument to `last()` must be ARRAY! Received %s instead!", args[0].Type())
			}

			arrayObj := args[0].(*object.Array)
			if len(arrayObj.Elements) > 0 {
				return arrayObj.Elements[len(arrayObj.Elements)-1]
			}

			return NULL
		},
	},
	"rest": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			if len(args) != 1 {
				return newError("Wrong number of arguments! Expected: 1; received: %d!", len(args))
			}

			if args[0].Type() != object.ARRAY_OBJ {
				return newError("Argument to `rest()` must be ARRAY! Received %s instead!", args[0].Type())
			}

			arrayObj := args[0].(*object.Array)
			length := len(arrayObj.Elements)
			if length > 0 {
				newElements := make([]object.Object, length-1, length-1)
				copy(newElements, arrayObj.Elements[1:length])
				return &object.Array{Elements: newElements}
			}

			return NULL
		},
	},
	"push": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			if len(args) != 2 {
				return newError("Wrong number of arguments! Expected: 2; received: %d!", len(args))
			}

			if args[0].Type() != object.ARRAY_OBJ {
				return newError("First argument to `push()` must be ARRAY! Received %s instead!", args[0].Type())
			}

			arrayObj := args[0].(*object.Array)
			length := len(arrayObj.Elements)

			newElements := make([]object.Object, length+1, length+1)
			copy(newElements, arrayObj.Elements)
			newElements[length] = args[1]

			return &object.Array{Elements: newElements}
		},
	},
	"puts": &object.Builtin{
		Fn: func(args ...object.Object) object.Object {
			for _, arg := range args {
				fmt.Println(arg.Inspect())
			}

			return NULL
		},
	},
}
