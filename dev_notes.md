# Table of Contents
[Notes](#notes)  
[TODO](#todo)  

<a name="notes"></a>
# Notes

## Key Concepts

- Make sure to rename the project from `chameleon` to `chameleon-lang`
- Pratt Parser:
  - Associate token types with parsing functions
- _tree-walking_ interpreter is the most basic one
  - recursively evaluates an AST
- Some interpreters also traverse the AST, but instead of interpreting the AST,
  - they first convert it to bytecode
  - can't be executed directly,
  - instead, the bytecode is _interpreted_ by VM
  - greater performance
- **JIT** interpreter/compiler
  - parse the source code,
  - ...build an AST and convert this AST to bytecode,
  - ...the VM then compiles the bytecode to natvie machine code,
  - ...right before it's executed - just in time
- Representing Objects
  - we need a system that can represent
    - the values our AST represents
    - or, values we generate when evaluating AST in memory
    - using the host language..?
- `...` in Go
  - **last** parameter of a function definition:
    - _any_ number of trailing arguments of type `T`
    - the actual type of `...T` is `[]T`
  - unpack/destructure a slice:
    - `arguments...`
  - in an array literal, `...` specifies a length equal to the number of elements
    in the literal
    - `someArray := [...]string{"Hello, ", "world...", " ..bye"}`
  - used by the `go` command, as wildcard when describing package lists
    `go test ./...`
- **the Environment**
  - it's what we use to keep track of values by associating them with a name
  - under the hood it's just a hash map that associates strings with objects
- `func copy(dst, src []Type) int`
-

## Design Decisions

- only letters and underscore are allowed in variable names
- semicolons after an expression or a statement should be optional
- `if-else`
  - `if (<condition>) <consequence> else <alternative>`
  - `<consequence>` and `<alternative>` are _block statements_
- tokens get advanced **just enough**
- `expectPeek()` implicitly advances token
- _function literals_

```
fn(x, y) {
  return x + y;
}
```

- `fn <parameters> <block statement>`
- _parameters_
  - `(<param one>, <param two>, ...)`
  - can also be empty
- function literal is actually an `ast.Expression`
- function literal can be an argument of another function
- consists of three parts:
  - check `*ast.FunctionLiteral` is there
  - check the parameters list is correct
  - body contains the correct statement(s)
- Calling expressions
  - `<expression>(<comma separated expressions>);`
  -
- We are going to represent every value we encounter during evaluation
  - ...as an `Object`
  - , an interface of our design
- Chameleon supports two prefix operators: `!` and `-`
- we compare `Boolean`s by comparing their pointers
  - faster than comparison between `Integer` objects
  - -> we have to unwrap `Integer` objects first
- What's considered _truthy_?
  - not null
  - not `false`
- Error Handling:
  - _Real_ error handling:
    - **NOT** user-defined exceptions
    - but internal errors
  - Actually does two things:
    - emit errors for unsupported operations
    - prevent further evaluations
  - We should check for errors whenever we call `Eval` inside of `Eval`, in
    order to stop errors from being passed around and bubbling up too far
- We define a wrapper, `Environment`, a thin wrapper around `map`
- For testing, we don't want to keep track of variable bindings between different tests
  - each call to `testEval` should have a fresh environment
- Functions carry their own environment with them
  - allows for **closures**
- **Extend the environment**
  - when evaluating function body with arguments
  - we want to preserve previous bindings while at the same time making new ones available
  - -> create a new instance of `object.Environment` with a pointer to the environment it should extend
  - by doing that, we enclose a fresh and empty environment with an existing one
  - `outer` is the enclosing environment, which is the one we are extending
  - think about _variable scopes_
    - the _inner_ scope extends the _outer_ scope
  - but we _do not_ use the current environment as the enclosing/outer env,
  - ...instead we use the env that `*object.Function` carries around
- **Closure** is enabled!
  - _closure_ is function that closes over the environment it's defined in
- Add support for **string**
  - from the lexer: a single token for each string literal
- **Builtin Functions**
  - why not move the error handling part to `TestErrorHandling`?
  - Add `strlen` as the built-in function to return the length of a string
  - Keep a separate environment for builtin functions!
  - we need to modify the `evalIdentifier` function to lookup built-in functions as a fallback
- Array
  - an ordered list of elements of possibly different types
  - add `arrlen` to calculate length of array
  1. the first step: add tokens "[" and "]"
  2. next, add tests to `lexer_test.go`
  3. generate the corresponding token in `lexer.go`
  4. add the AST node for `ArrayLiteral`
  5. write tests to make sure parsing arrays indeed return `ArrayLiteral`
  6. register a new `prefixParseFunc`
  - Parsing index operator expressions
    - basic syntax: `<expression>[<expression>]`
    1. Add a new AST node, `ast.IndexExpression`
    2. Make sure the parser handles the precedence of the index operator correctly - highest precedence
    3. Treat the index operator expression as an `InfixExpression`
    4. Register `parseIndexExpression` to `registerInfix`
    5. Remember to give `token.LBRACKET` the highest precedence in `parser.go`
  - Evaluate array literals
    1. add a new `Array` type to the object system
    2. add test
    3. add another branch for the `switch` in `Eval()`
    - REMEMBER to check the off-by-one error!
    - Return `NULL` for out-of-range indexing
  - Arrays are **immutable** in _chameleon_
- Hash
  - basic syntax:
    - `{<expression>: <expression>, <expression>: <expression>, ...}`
    - the only three data types allowed as a key:
      - `string`
      - `integer`
      - `boolean`
    - but we can't enforce the type check during parsing
    - ...because that would prevent us from evaluating expressions into those three types
    - instead, we do that during evaluation
  1. Turn **hash literals** into `token`s
  2. the only token left is `:`
  3. Make sure all literal have their corresponding tokens
  4. Add `HashLiteral` to the AST tree
  5. Parse `HashLiteral`
  6. Represent the new data type, `HashLiteral`, in the object system
  7. use the package `hash/fnv` to generate hash keys for different data types
    - **hash collisions** may happen
    - when saving/accessing the key-value pairs, we want to save the original key object
  8. Add a new branch to `Eval()`

## Misc

- `Token` & `TokenType`
  - `Type` is `string`
- Ideas behind Pratt parser
  - the association of parsing functions with token types
  - each token type can have up to _two_ parsing functions
    - whether the token is in _prefix_ or _infix_
- _identifiers_ produce a value: they evaluate to the value they are bound to
- infix operators in binary expressions
- prefix operators in unary expressions
- try _not_ to use pointer to interface
- **everything in Go is passed by value**
- We do **not** have function `currTokenIs`
- when printing:
  - `%v` prints the value in default format
  - `%+v` adds field names for structs
  - `%T` a Go-syntax representation of the type of the value
- Type switch
  - `switch t := t.(type)`
  - discover the dynamic type of an interface variable
- `switch`:
  - can be used without anything: equivalent to `switch true` maybe?
- _Need to do type assertion in function arguments list!_
- Notice the differences between `%s` and `%q`
  - `%q` is fully escaped, with double quotes!
  - `%s` is uninterpreted bytes

<a name="todo"></a>
# TODO

- Add support for `<=` and `>=`
