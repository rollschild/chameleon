package main

import (
	"chameleon-lang/repl"
	"fmt"
	"os"
	"os/user"
)

func main() {
	user, err := user.Current()
	if err != nil {
		panic(err)
	}

	fmt.Printf("Hello %s, this is the Chamemelon Programming Languge.\n", user.Username)
	fmt.Printf("Type ANYTHING...\n")
	repl.StartRepl(os.Stdin, os.Stdout)
}
