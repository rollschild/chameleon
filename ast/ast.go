package ast

// for the AST tree

import (
	"bytes"
	"chameleon-lang/token"
	"strings"
)

// Node
// a collection of method signatures
// EVERY node type in package ast has to implement each of the
// ...following
type Node interface {
	TokenLiteral() string
	String() string
}

// the point of the String() is to return the whole program
// ...as a string
// ...easy to test

// Statement is exported
type Statement interface {
	Node
	statementNode()
}

type Expression interface {
	Node
	expressionNode()
}

// Program node is the root node of every AST
type Program struct {
	Statements []Statement
	// a slice of AST nodes that implement the Statement interface
}

// the string literal of the token
func (p *Program) TokenLiteral() string {
	if len(p.Statements) > 0 {
		return p.Statements[0].TokenLiteral()
	} else {
		return ""
	}
}

func (p *Program) String() string {
	var out bytes.Buffer // notice the declaration

	for _, stmt := range p.Statements {
		out.WriteString(stmt.String())
	}

	return out.String()
}

// it implements the Statement interface
type LetStatement struct {
	Token token.Token // token.LET token
	Name  *Identifier // the variable name following let
	Value Expression
}

func (ls *LetStatement) statementNode() {}
func (ls *LetStatement) TokenLiteral() string {
	return ls.Token.Literal
}
func (ls *LetStatement) String() string {
	var out bytes.Buffer

	out.WriteString(ls.TokenLiteral() + " ")
	out.WriteString(ls.Name.String())
	out.WriteString(" = ")

	if ls.Value != nil {
		out.WriteString(ls.Value.String())
	}

	out.WriteString(";")

	return out.String()
}

type ReturnStatement struct {
	Token       token.Token // token.RETURN
	ReturnValue Expression
}

func (rs *ReturnStatement) statementNode() {}
func (rs *ReturnStatement) TokenLiteral() string {
	return rs.Token.Literal
}
func (rs *ReturnStatement) String() string {
	var out bytes.Buffer

	out.WriteString(rs.TokenLiteral() + " ")

	if rs.ReturnValue != nil {
		out.WriteString(rs.ReturnValue.String())
	}

	out.WriteString(";")

	return out.String()
}

type ExpressionStatement struct {
	Token           token.Token // first token of the Expression
	ExpressionValue Expression
}

// implements interface Statement
func (es *ExpressionStatement) statementNode() {}
func (es *ExpressionStatement) TokenLiteral() string {
	return es.Token.Literal
}
func (es *ExpressionStatement) String() string {
	if es.ExpressionValue != nil {
		return es.ExpressionValue.String()
	} else {
		return ""
	}
}

// we reuse Identifier for both Identifier and Expression
type Identifier struct {
	Token token.Token
	Value string
}

func (id *Identifier) expressionNode() {}
func (id *Identifier) TokenLiteral() string {
	return id.Token.Literal
}
func (id *Identifier) String() string {
	return id.Value
}

type IntegerLiteral struct {
	Token token.Token
	Value int64
}

func (il *IntegerLiteral) expressionNode() {}
func (il *IntegerLiteral) TokenLiteral() string {
	return il.Token.Literal
}
func (il *IntegerLiteral) String() string {
	return il.Token.Literal
}

type StringLiteral struct {
	Token token.Token
	Value string
}

func (sl *StringLiteral) expressionNode() {}
func (sl *StringLiteral) TokenLiteral() string {
	return sl.Token.Literal
}
func (sl *StringLiteral) String() string {
	return sl.Token.Literal
}

type PrefixExpression struct {
	Token    token.Token
	Operator string
	ExprVal  Expression
}

func (pe *PrefixExpression) expressionNode() {}
func (pe *PrefixExpression) TokenLiteral() string {
	return pe.Token.Literal
}
func (pe *PrefixExpression) String() string {
	var output bytes.Buffer

	output.WriteString("(")
	output.WriteString(pe.Operator)
	output.WriteString(pe.ExprVal.String())
	output.WriteString(")")

	return output.String()
}

type InfixExpression struct {
	Token      token.Token // the infix operator token. e.g. `+`
	LeftValue  Expression
	Operator   string
	RightValue Expression
}

func (ie *InfixExpression) expressionNode() {}
func (ie *InfixExpression) TokenLiteral() string {
	return ie.Token.Literal
}
func (ie *InfixExpression) String() string {
	var output bytes.Buffer

	output.WriteString("(")
	output.WriteString(ie.LeftValue.String())
	output.WriteString(" " + ie.Operator + " ")
	output.WriteString(ie.RightValue.String())
	output.WriteString(")")

	return output.String()
}

// Boolean type
type Boolean struct {
	Token token.Token
	Value bool // true or false
}

func (b *Boolean) expressionNode() {}
func (b *Boolean) TokenLiteral() string {
	return b.Token.Literal
}
func (b *Boolean) String() string {
	return b.Token.Literal
}

type IfExpression struct {
	Token       token.Token // keyword *if*
	Condition   Expression
	Consequence *BlockStatement
	Alternative *BlockStatement
}

func (ie *IfExpression) expressionNode() {}
func (ie *IfExpression) TokenLiteral() string {
	return ie.Token.Literal
}
func (ie *IfExpression) String() string {
	var output bytes.Buffer

	output.WriteString("if ")
	output.WriteString(ie.Condition.String())
	output.WriteString(" ")
	output.WriteString(ie.Consequence.String())

	if ie.Alternative != nil {
		output.WriteString(" else ")
		output.WriteString(ie.Alternative.String())
	}

	return output.String()
}

type BlockStatement struct {
	Token      token.Token // {
	Statements []Statement
}

func (bs *BlockStatement) statementNode() {}
func (bs *BlockStatement) TokenLiteral() string {
	return bs.Token.Literal
}
func (bs *BlockStatement) String() string {
	var output bytes.Buffer

	for _, stmt := range bs.Statements {
		output.WriteString(stmt.String())
	}

	return output.String()
}

type FunctionLiteral struct {
	Token      token.Token // keyword *fn*
	Parameters []*Identifier
	Body       *BlockStatement
}

func (fl *FunctionLiteral) expressionNode() {}
func (fl *FunctionLiteral) TokenLiteral() string {
	return fl.Token.Literal
}
func (fl *FunctionLiteral) String() string {
	var output bytes.Buffer

	params := []string{} // slice of string
	for _, param := range fl.Parameters {
		params = append(params, param.String())
	}

	output.WriteString(fl.TokenLiteral())
	output.WriteString("(")
	output.WriteString(strings.Join(params, ", "))
	output.WriteString(")")
	output.WriteString(fl.Body.String())

	return output.String()
}

type CallExpression struct {
	Token token.Token
	// the ( token, acting as the operator in an infix expression

	Function  Expression // Identifier or FunctionLiteral
	Arguments []Expression
}

func (ce *CallExpression) expressionNode() {}
func (ce *CallExpression) TokenLiteral() string {
	return ce.Token.Literal
}
func (ce *CallExpression) String() string {
	var output bytes.Buffer

	args := []string{}
	for _, arg := range ce.Arguments {
		args = append(args, arg.String())
	}

	output.WriteString(ce.Function.String())
	output.WriteString("(")
	output.WriteString(strings.Join(args, ", "))
	output.WriteString(")")

	return output.String()
}

type ArrayLiteral struct {
	Token    token.Token // "["
	Elements []Expression
}

func (al *ArrayLiteral) expressionNode() {}
func (al *ArrayLiteral) TokenLiteral() string {
	return al.Token.Literal
}
func (al *ArrayLiteral) String() string {
	var output bytes.Buffer

	elements := []string{}
	for _, element := range al.Elements {
		elements = append(elements, element.String())
	}

	output.WriteString("[")
	output.WriteString(strings.Join(elements, ", "))
	output.WriteString("]")

	return output.String()
}

type IndexExpression struct {
	Token token.Token // the [ token
	Host  Expression
	Index Expression
}

func (ie *IndexExpression) expressionNode() {}
func (ie *IndexExpression) TokenLiteral() string {
	return ie.Token.Literal
}
func (ie *IndexExpression) String() string {
	var output bytes.Buffer

	output.WriteString("(")
	output.WriteString(ie.Host.String())
	output.WriteString("[")
	output.WriteString(ie.Index.String())
	output.WriteString("])")

	return output.String()
}

type HashLiteral struct {
	Token token.Token // the { token
	Pairs map[Expression]Expression
}

func (hl *HashLiteral) expressionNode() {}
func (hl *HashLiteral) TokenLiteral() string {
	return hl.Token.Literal
}
func (hl *HashLiteral) String() string {
	var output bytes.Buffer

	pairs := []string{}
	for key, value := range hl.Pairs {
		pairs = append(pairs, key.String()+": "+value.String())
	}

	output.WriteString("{")
	output.WriteString(strings.Join(pairs, ", "))
	output.WriteString("}")

	return output.String()
}
