package ast

import (
	"chameleon-lang/token"
	"testing"
)

func TestString(t *testing.T) {
	program := &Program{
		Statements: []Statement{
			&LetStatement{
				Token: token.Token{Type: token.LET, Literal: "let"},
				Name: &Identifier{
					Token: token.Token{
						Type:    token.IDENTIFIER,
						Literal: "myVarr",
					},
					Value: "myVarr",
				},
				Value: &Identifier{
					Token: token.Token{
						Type:    token.IDENTIFIER,
						Literal: "someVarr",
					},
					Value: "someVarr",
				},
			},
		},
	}

	if program.String() != "let myVarr = someVarr;" {
		t.Errorf("String value of the program is wrong! Received %q instead...", program.String())
	}

}
